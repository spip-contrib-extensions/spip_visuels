<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'cfg_types_visuels_label' => 'Type of pictures',
	'cfg_types_visuels_explication' => 'A list <strong>id;label</strong> separated by line breaks.<br/><u>Example :</u><br>home_picture;Picture for homepage<br />aside_picture;Picture for the aside',
	'cfg_titre_parametrages' => 'Configurations',

	// F
	'form_choisir_role' => 'Select a role for this picture',
	'form_role_aucun' => 'None',
	'form_ajouter_visuel' => 'Add a picture',
	'form_choisir_fichier' => 'Choose a file',
	'form_btn_televerser' => 'Upload',

	// S
	'spip_visuels_titre' => 'Pictures',
	'supprimer_visuel' => 'Remove picture',

	// T
	'titre_page_configurer_spip_visuels' => 'Configure pictures',
	'titre_boite' => 'Pictures for this article',
);

?>