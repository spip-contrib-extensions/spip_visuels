<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'spip_visuels_description' => 'Gestion de visuels pour les articles selon leur contexte d\'affichage sur le site public',
	'spip_visuels_nom' => 'Visuels',
	'spip_visuels_slogan' => '',
);

?>